/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */
#ifndef __HIPPO_COMMON_H__
#define __HIPPO_COMMON_H__

#include <glib-object.h>

G_BEGIN_DECLS

#include <hippo/hippo-basics.h>
#include <hippo/hippo-entity.h>
#include <hippo/hippo-feed.h>
#include <hippo/hippo-group.h>
#include <hippo/hippo-person.h>

G_END_DECLS

#endif /* __HIPPO_COMMON_H__ */
