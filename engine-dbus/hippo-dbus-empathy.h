/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */
#ifndef __HIPPO_DBUS_EMPATHY_H__
#define __HIPPO_DBUS_EMPATHY_H__

#include <glib.h>

G_BEGIN_DECLS

void hippo_dbus_init_empathy(void);

G_END_DECLS

#endif /* __HIPPO_DBUS_PIDGIN_H__ */
