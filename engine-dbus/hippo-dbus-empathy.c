/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */
#include <config.h>

/* Only compile with Empathy support if it is enabled */
#ifdef HAVE_LIBEMPATHY

#include <string.h>
#include <libempathy/empathy-contact-manager.h>
#include <libempathy/empathy-contact-list.h>
#include "hippo-im.h"
#include "hippo-dbus-empathy.h"

static char *
make_buddy_resource_id(EmpathyContact *contact)
{
    McAccount *account = empathy_contact_get_account(contact);
    McProfile *profile = mc_account_get_profile(account);

    const char *protocol = mc_profile_get_protocol_name(profile);
    const char *id = empathy_contact_get_id(contact);

    if (protocol == NULL || id == NULL)
        return NULL;
    
    return g_strdup_printf("online-desktop:/o/empathy-buddy/%s.%s", protocol, id);
}

static void
update_buddy(EmpathyContact *contact)
{
    EmpathyAvatar *avatar;
    McAccount *account;
    McProfile *profile;
    const char *protocol;

    char *resource_id = make_buddy_resource_id(contact);
    if (resource_id == NULL)
        return;

    account = empathy_contact_get_account(contact);
    profile = mc_account_get_profile(account);
    protocol = mc_profile_get_protocol_name(profile);

    if (strcmp(protocol, "jabber") == 0)
        protocol = "xmpp";

    hippo_im_update_buddy(resource_id,
                          protocol,
                          empathy_contact_get_id(contact),
                          empathy_contact_get_name(contact),
                          empathy_contact_is_online(contact),
                          empathy_contact_get_status(contact),
                          empathy_contact_get_presence_message(contact),
                          NULL /* webdav_url */);

    avatar = empathy_contact_get_avatar(contact);
    if (avatar != NULL) {
        hippo_im_update_buddy_icon(empathy_contact_get_id(contact), avatar->format,
                                   avatar->token, (gchar *)avatar->data, avatar->len);
    }

    g_free(resource_id);
    g_object_unref (profile);
}

static void
contact_updated_cb (EmpathyContact *contact,
                    GParamSpec     *param,
                    gpointer        data)
{
    update_buddy(contact);
}

static void
contact_list_members_changed_cb (EmpathyContactList *list_iface,
                                 EmpathyContact     *contact,
                                 EmpathyContact     *actor,
                                 guint               reason,
                                 gchar              *message,
                                 gboolean            is_member,
                                 gpointer            data)
{
    McAccount *account;
    McProfile *profile;
    McPresence presence;

    account = empathy_contact_get_account(contact);
    profile = mc_account_get_profile(account);
    presence = empathy_contact_get_presence(contact);

    if (is_member) {
        update_buddy(contact);

        g_signal_connect(contact, "notify::presence",
                         G_CALLBACK (contact_updated_cb), NULL);
        g_signal_connect(contact, "notify::presence-message",
                         G_CALLBACK (contact_updated_cb), NULL);
        g_signal_connect(contact, "notify::name",
                         G_CALLBACK (contact_updated_cb), NULL);
        g_signal_connect(contact, "notify::avatar",
                         G_CALLBACK (contact_updated_cb), NULL);
    } else {
        char *resource_id;
        g_signal_handlers_disconnect_by_func(contact,
                                             G_CALLBACK (contact_updated_cb), NULL);


        resource_id = make_buddy_resource_id(contact);
        if (resource_id != NULL) {
            hippo_im_remove_buddy(resource_id);
            g_free(resource_id);
        }
    }
}
#endif /* HAVE_LIBEMPATHY */

void
hippo_dbus_init_empathy(void)
{
#ifdef HAVE_LIBEMPATHY
    EmpathyContactManager *manager;

    manager = empathy_contact_manager_new ();

    g_signal_connect (manager,
                      "members-changed",
                      G_CALLBACK (contact_list_members_changed_cb),
                      NULL);
#endif
}
