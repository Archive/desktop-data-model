/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */
#include <config.h>
#include "hippo-cookies-linux.h"
#include <string.h>
#include <libgnomevfs/gnome-vfs.h>
#include <engine/hippo-cookies.h>

typedef struct {
    HippoCookiesMonitorFunc func;
    void                   *data;    
} CookieMonitor;

typedef enum {
    MONITOR_FILE,
    MONITOR_DIRECTORY,
    MONITOR_SUBDIRECTORY
} MonitorType;

typedef struct {
    char *path;
    MonitorType monitor_type;
    GnomeVFSMonitorHandle *handle;
} MonitoredCookieFile;

static void add_monitored_cookie_file    (const char  *path,
                                          MonitorType  monitor_type);
static void remove_monitored_cookie_file (const char  *path);

static int cookie_monitors_serial = 0;
static GSList *cookie_monitors = NULL;
static GHashTable *monitored_files = NULL;

static void
cookie_monitors_notify(void)
{
    GSList *l;
    int start_serial;
        
    start_serial = cookie_monitors_serial;
    for (l = cookie_monitors; l != NULL; l = l->next) {
        CookieMonitor *cm = l->data;

        g_assert(cm != NULL);
        
        (* cm->func) (cm->data);
        
        if (start_serial != cookie_monitors_serial) {
            /* This is not supposed to happen, the warning is here in case we
             * ever accidentally create the bug
             */
            g_warning("Cookie monitor added/removed while notifying cookie monitors");
            return;
        }
    }
}

static void
add_subdirectory_monitors(const char *path)
{
    GDir *dir = g_dir_open(path, 0, NULL);
    if (dir == NULL)
        return;

    while (TRUE) {
        const char *name = g_dir_read_name(dir);
        char *fullname;
        if (name == NULL)
            return;

        fullname = g_build_filename(path, name, NULL);
        if (g_file_test(fullname, G_FILE_TEST_IS_DIR))
            add_monitored_cookie_file(fullname, MONITOR_SUBDIRECTORY);
            
    }

    g_dir_close(dir);
}

static char *
path_from_info_uri(const char *info_uri)
{
    char *local_path;
    int len;
    
    if (info_uri == NULL)
        return NULL;
    
    local_path = gnome_vfs_get_local_path_from_uri(info_uri);
    if (local_path == NULL) /* Can't convert to a local path */
        return NULL;
    
    len = strlen(local_path);
    /* If the directory itself is created/deleted, then the path has a trailing
     * '/' currently; rather than counting on that, we just strip it
     */
    if (local_path[len - 1] == '/')
        local_path[len - 1] = '\0';

    return local_path;
}

static void
on_cookie_file_changed(GnomeVFSMonitorHandle *handle,
                       const gchar *monitor_uri,
                       const gchar *info_uri,
                       GnomeVFSMonitorEventType event_type,
                       gpointer user_data)
{
    MonitoredCookieFile *mcf = user_data;

    switch (mcf->monitor_type) {
    case MONITOR_DIRECTORY:
        /* .mozilla/firefox:
         *    if the directory itself is created, add monitors on all subdirectories,
         *       and reread cookies
         *    if a subdirectory is created, add a monitor on it and reread cookies
         */
        switch (event_type) {
        case GNOME_VFS_MONITOR_EVENT_CREATED:
            {
                char *local_path = path_from_info_uri(info_uri);
                if (local_path == NULL)
                    return;
                
                if (strcmp(local_path, mcf->path) == 0) {
                    add_subdirectory_monitors(local_path);
                } else if (g_file_test(local_path, G_FILE_TEST_IS_DIR))
                    add_monitored_cookie_file(local_path, MONITOR_SUBDIRECTORY);
                
                cookie_monitors_notify();

                g_free(local_path);
            }
            break;
        case GNOME_VFS_MONITOR_EVENT_DELETED:
            {
                /* Deleting or moving aside the directory itself causes problems for
                 * a number of reasons
                 *
                 * - Notify watches on the children don't fire at least in the
                 *   move-aside case (inotify bug or misfeature?)
                 * - gnome-vfs gets into a weird state (not fully diagnosed, some
                 *   combination of gnome-vfs and inotify problems)
                 * - We don't clean out the child watches, so they won't get added
                 *   back, but the old ones may won't work.
                 *
                 * So, if someone rm -rf's ~/.mozilla, the Online Desktop may not work
                 * until they log out and log back again. Tough.
                 */
                char *local_path = path_from_info_uri(info_uri);
                if (strcmp(local_path, mcf->path) == 0)
                    g_warning("Directory of profiles %s removed, future notification may to work", local_path);
            }
        case GNOME_VFS_MONITOR_EVENT_CHANGED:
        case GNOME_VFS_MONITOR_EVENT_STARTEXECUTING:
        case GNOME_VFS_MONITOR_EVENT_STOPEXECUTING:
        case GNOME_VFS_MONITOR_EVENT_METADATA_CHANGED: 
            break;
        }
    case MONITOR_SUBDIRECTORY:
        /* .mozilla/firefox/550taoaa.default:
         *    if a file in the directory called cookies.txt or cookies.sqlite is created
         *     changed, or delete, reread cookies
         *    if the directory itself is removed, remove the monitor, and reread cookies
         */
        switch (event_type) {
        case GNOME_VFS_MONITOR_EVENT_CREATED:
        case GNOME_VFS_MONITOR_EVENT_CHANGED:
            if (g_str_has_suffix(info_uri, "/cookies.txt") ||
                g_str_has_suffix(info_uri, "/cookies.sqlite"))
                cookie_monitors_notify();
            break;
        case GNOME_VFS_MONITOR_EVENT_DELETED:
            {
                char *local_path = path_from_info_uri(info_uri);
                if (local_path == NULL)
                    return;
                
                if (strcmp(local_path, mcf->path) == 0) {
                    remove_monitored_cookie_file(local_path);
                    cookie_monitors_notify();
                } else {
                    if (g_str_has_suffix(info_uri, "/cookies.txt") ||
                        g_str_has_suffix(info_uri, "/cookies.sqlite"))
                        cookie_monitors_notify();
                }
                
                g_free(local_path);
            }
            break;
        case GNOME_VFS_MONITOR_EVENT_STARTEXECUTING:
        case GNOME_VFS_MONITOR_EVENT_STOPEXECUTING:
        case GNOME_VFS_MONITOR_EVENT_METADATA_CHANGED: 
            break;
        }
        break;
    case MONITOR_FILE:
        /* .galeon/mozilla/galeon/cookies.txt
         *    if the file is created, changed, or deleted, reread cookies
         */
        switch (event_type) {
        case GNOME_VFS_MONITOR_EVENT_CREATED:
        case GNOME_VFS_MONITOR_EVENT_CHANGED:
        case GNOME_VFS_MONITOR_EVENT_DELETED:
            cookie_monitors_notify();
            break;
        case GNOME_VFS_MONITOR_EVENT_STARTEXECUTING:
        case GNOME_VFS_MONITOR_EVENT_STOPEXECUTING:
        case GNOME_VFS_MONITOR_EVENT_METADATA_CHANGED: 
            break;
        }
    }
}

static void
add_monitored_cookie_file(const char  *path,
                          MonitorType  monitor_type)
{
    MonitoredCookieFile *mcf;
    GnomeVFSResult result;
    
    if (monitored_files == NULL) {
        monitored_files = g_hash_table_new(g_str_hash, g_str_equal);
    }

    if (g_hash_table_lookup(monitored_files, path) != NULL) {
        /* already monitored */
        return;
    }
    
    /* This is idempotent and fairly cheap, so do it here to avoid initializing
     * gnome-vfs on application startup
     */
    gnome_vfs_init();

    mcf = g_new0(MonitoredCookieFile, 1);

    mcf->path = g_strdup(path);
    mcf->monitor_type = monitor_type;

    result = gnome_vfs_monitor_add(&mcf->handle,
                                   mcf->path,
                                   monitor_type == MONITOR_FILE ? GNOME_VFS_MONITOR_FILE : GNOME_VFS_MONITOR_DIRECTORY,
                                   on_cookie_file_changed,
                                   mcf);
    if (result != GNOME_VFS_OK) {
        g_warning("Failed to monitor cookie file '%s'", mcf->path);
        g_free(mcf->path);
        g_free(mcf);
        return;
    }
    
    g_hash_table_replace(monitored_files, mcf->path, mcf);

    if (monitor_type == MONITOR_DIRECTORY)
        add_subdirectory_monitors(path);
}

static void
remove_monitored_cookie_file(const char *path)
{
    MonitoredCookieFile *mcf;
    
    if (monitored_files == NULL)
        return;
    
    mcf = g_hash_table_lookup(monitored_files, path);
    if (mcf == NULL)
        return;

    g_hash_table_remove(monitored_files, path);
        
    gnome_vfs_monitor_cancel(mcf->handle);
    g_free(mcf->path);
    g_free(mcf);
}

GSList*
hippo_load_cookies(const char *domain,
                   int         port,
                   const char *name)
{
    HippoCookieLocator *locator = hippo_cookie_locator_new();
    const char *homedir = g_get_home_dir();
    char *path;
    GSList *cookies;

    /* We load the epiphany cookies, and the cookies from all profiles of firefox; 
     * not really clear what we "should" do, how do we know which browser is someone's 
     * "main" or "current"? I guess if any browser has our login cookie, the user's 
     * account is effectively logged in from a security standpoint...
     */
    path = g_build_filename(homedir,
                            ".gnome2/epiphany/mozilla/epiphany/cookies.txt",
                            NULL);
    hippo_cookie_locator_add_file(locator, path, HIPPO_BROWSER_EPIPHANY);
    add_monitored_cookie_file(path, MONITOR_FILE);
    g_free(path);

    path = g_build_filename(homedir,
                            ".galeon/mozilla/galeon/cookies.txt",
                            NULL);
    hippo_cookie_locator_add_file(locator, path, HIPPO_BROWSER_GALEON);
    add_monitored_cookie_file(path, MONITOR_FILE);
    g_free(path);

    path = g_build_filename(homedir,
                            ".mozilla/microb/cookies.txt",
                            NULL);
    hippo_cookie_locator_add_file(locator, path, HIPPO_BROWSER_MAEMO);
    add_monitored_cookie_file(path, MONITOR_FILE);
    g_free(path);
    
    path = g_build_filename(homedir, ".mozilla/firefox", NULL);
    hippo_cookie_locator_add_directory(locator, path, HIPPO_BROWSER_FIREFOX);
    add_monitored_cookie_file(path, MONITOR_DIRECTORY);
    g_free(path);
    
    path = g_build_filename(homedir, ".firefox", NULL);
    hippo_cookie_locator_add_directory(locator, path, HIPPO_BROWSER_FIREFOX);
    add_monitored_cookie_file(path, MONITOR_DIRECTORY);
    g_free(path);

    cookies = hippo_cookie_locator_load_cookies(locator, domain, port, name);
    
    g_debug("Loaded %d cookies matching domain '%s' port %d cookie name '%s'",
            g_slist_length(cookies), domain, port, name);

    hippo_cookie_locator_destroy(locator);
    
    return cookies;
}

void
hippo_cookie_monitor_add (HippoCookiesMonitorFunc  func,
                          void                    *data)
{
    CookieMonitor *cm;

    cm = g_new0(CookieMonitor, 1);
    cm->func = func;
    cm->data = data;
    cookie_monitors = g_slist_append(cookie_monitors, cm);

    ++cookie_monitors_serial;
}

void
hippo_cookie_monitor_remove (HippoCookiesMonitorFunc  func,
                             void                    *data)
{
    GSList *l;

    for (l = cookie_monitors; l != NULL; l = l->next) {
        CookieMonitor *cm = l->data;

        if (cm->func == func && cm->data == data) {
            cookie_monitors = g_slist_remove(cookie_monitors, cm);
            ++cookie_monitors_serial;
            return;
        }
    }

    g_warning("Attempt to remove cookie monitor that was not found");
}

#if 0
static void
print_and_eat_cookies(GSList *cookies)
{
    int count;
    
    count = 1;
    while (cookies != NULL) {
        HippoCookie *cookie = cookies->data;

        g_print("%d '%s:%d' all_hosts=%d path='%s' secure=%d time=%lu name='%s' value='%s'\n", count,
            hippo_cookie_get_domain(cookie),
            hippo_cookie_get_port(cookie),
            hippo_cookie_get_all_hosts_match(cookie),
            hippo_cookie_get_path(cookie),
            hippo_cookie_get_secure_connection_required(cookie),
            (unsigned long) hippo_cookie_get_timestamp(cookie),
            hippo_cookie_get_name(cookie),
            hippo_cookie_get_value(cookie));
        
        count += 1;
        
        cookies = g_slist_remove(cookies, cookies->data);
        hippo_cookie_unref(cookie);
    }
}

int
main(int argc, char **argv)
{
    GSList *cookies;
    int i;
    
    cookies = hippo_load_cookies(NULL, -1, NULL);
    print_and_eat_cookies(cookies);
    
    for (i = 1; i < argc; ++i) {
        GError *error = NULL;
        cookies = hippo_load_cookies_file(argv[i], NULL, -1, NULL, &error);
        if (error != NULL) {
            g_printerr("Failed to load '%s': %s\n", argv[i], error->message);
            g_error_free(error);
        }
        
        g_print("=== %s === ", argv[i]);   
        print_and_eat_cookies(cookies);
    }
    
    return 0;
}
#endif
