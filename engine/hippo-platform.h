/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */
#ifndef __HIPPO_PLATFORM_H__
#define __HIPPO_PLATFORM_H__

#include <engine/hippo-engine-basics.h>

G_BEGIN_DECLS

typedef struct {
    const char *name;         /* "windows" or "linux" */
    const char *distribution; /* Not used on Windows. "Fedora", "Ubuntu", etc, on Linux */
    const char *version;      /* May be NULL if we don't know anything better */
    const char *architecture; /* May be NULL for "unknown" */
} HippoPlatformInfo;

typedef enum {
    HIPPO_NETWORK_STATUS_UNKNOWN,
    HIPPO_NETWORK_STATUS_DOWN,
    HIPPO_NETWORK_STATUS_UP
} HippoNetworkStatus;

typedef struct _HippoPlatform      HippoPlatform;
typedef struct _HippoPlatformClass HippoPlatformClass;

#define HIPPO_TYPE_PLATFORM              (hippo_platform_get_type ())
#define HIPPO_PLATFORM(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), HIPPO_TYPE_PLATFORM, HippoPlatform))
#define HIPPO_PLATFORM_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), HIPPO_TYPE_PLATFORM, HippoPlatformClass))
#define HIPPO_IS_PLATFORM(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), HIPPO_TYPE_PLATFORM))
#define HIPPO_IS_PLATFORM_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), HIPPO_TYPE_PLATFORM))
#define HIPPO_PLATFORM_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_INTERFACE ((obj), HIPPO_TYPE_PLATFORM, HippoPlatformClass))

struct _HippoPlatformClass {
    GTypeInterface base_iface;

    void (* get_platform_info) (HippoPlatform     *platform,
                                HippoPlatformInfo *info);
    
    gboolean  (* read_login_cookie)   (HippoPlatform    *platform,
                                       HippoServerType   web_server_type,
                                       HippoBrowserKind *origin_browser_p,
                                       char            **username,
                                       char            **password);
    void      (* delete_login_cookie) (HippoPlatform  *platform);                                   
    
    const char* (* get_jabber_resource) (HippoPlatform  *platform);

    void      (* open_url)            (HippoPlatform   *platform,
                                       HippoBrowserKind browser,
                                       const char      *url);

    HippoNetworkStatus (* get_network_status) (HippoPlatform *platform);
    
    /* Preferences */
    char*     (* get_message_server)  (HippoPlatform  *platform,
                                       HippoServerType server_type);
    char*     (* get_web_server)      (HippoPlatform  *platform,
                                       HippoServerType server_type);
    gboolean  (* get_signin)          (HippoPlatform  *platform);
    
    void     (* set_message_server)  (HippoPlatform *platform, const char *value);
    void     (* set_web_server)      (HippoPlatform *platform, const char *value);
    void     (* set_signin)          (HippoPlatform *platform, gboolean    value);

    /* Create a suitable filename for storing the cache via SQLite; it should be
     * machine-specific or live in a location that is visible only to this machine
     * and not network mounted, since SQLite databases can't reliably be shared
     * between machines. */
    char *   (* make_cache_filename) (HippoPlatform  *platform,
                                      const char     *server,
                                      const char     *user_id);

    HippoInstanceType (* get_instance_type) (HippoPlatform *platform);
};

GType            hippo_platform_get_type               (void) G_GNUC_CONST;

void             hippo_platform_get_platform_info      (HippoPlatform     *platform,
                                                        HippoPlatformInfo *info);

gboolean         hippo_platform_read_login_cookie      (HippoPlatform    *platform,
                                                        HippoServerType   server_type,
                                                        HippoBrowserKind *origin_browser_p,
                                                        char            **username_p,
                                                        char            **password_p);
void             hippo_platform_delete_login_cookie    (HippoPlatform *platform);

const char*      hippo_platform_get_jabber_resource    (HippoPlatform *platform); 

void             hippo_platform_open_url               (HippoPlatform   *platform,
                                                        HippoBrowserKind browser,
                                                        const char      *url);

HippoNetworkStatus hippo_platform_get_network_status (HippoPlatform *platform);

void               hippo_platform_emit_network_status_changed (HippoPlatform *platform,
                                                               HippoNetworkStatus status);
void               hippo_platform_emit_cookies_maybe_changed  (HippoPlatform *platform);

/* Preferences */
HippoInstanceType hippo_platform_get_instance_type (HippoPlatform *platform);

char*            hippo_platform_get_message_server     (HippoPlatform  *platform,
                                                        HippoServerType server_type); 
char*            hippo_platform_get_web_server         (HippoPlatform  *platform,
                                                        HippoServerType server_type); 
gboolean         hippo_platform_get_signin             (HippoPlatform *platform); 

void             hippo_platform_set_message_server     (HippoPlatform  *platform,
                                                        const char     *value); 
void             hippo_platform_set_web_server         (HippoPlatform  *platform,
                                                        const char     *value); 
void             hippo_platform_set_signin             (HippoPlatform  *platform,
                                                        gboolean        value);

char *           hippo_platform_make_cache_filename    (HippoPlatform  *platform,
                                                        const char     *server,
                                                        const char     *user_id);

/* Convenience wrappers on get_server stuff that parse the host/port */
void             hippo_platform_get_message_host_port  (HippoPlatform  *platform,
                                                        HippoServerType server_type,
                                                        char          **host_p,
                                                        int            *port_p);
void             hippo_platform_get_web_host_port      (HippoPlatform  *platform,
                                                        HippoServerType server_type,
                                                        char          **host_p,
                                                        int            *port_p);

G_END_DECLS

#endif /* __HIPPO_PLATFORM_H__ */
