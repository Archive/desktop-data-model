/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */
#ifndef __HIPPO_ENGINE_H__
#define __HIPPO_ENGINE_H__

#include <glib-object.h>

G_BEGIN_DECLS

#include <hippo/hippo-common.h>
#include <engine/hippo-engine-basics.h>
#include <engine/hippo-chat-room.h>
#include <engine/hippo-connection.h>
#include <engine/hippo-cookies.h>
#include <engine/hippo-data-cache.h>
#include <engine/hippo-endpoint-proxy.h>
#include <engine/hippo-platform.h>
#include <engine/hippo-settings.h>
#include <engine/hippo-title-pattern.h>

G_END_DECLS

#endif /* __HIPPO_ENGINE_H__ */
