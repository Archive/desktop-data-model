dnl -*- mode: m4 -*-
AC_PREREQ(2.59)

AC_INIT(ddm/ddm.h)
AC_CONFIG_AUX_DIR(config)

AC_CANONICAL_TARGET

AM_INIT_AUTOMAKE(desktop-data-model, 1.2.5)

AM_CONFIG_HEADER(config/config.h)

# Honor aclocal flags
ACLOCAL="$ACLOCAL $ACLOCAL_FLAGS"

GETTEXT_PACKAGE=mugshot
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE",[The name of the gettext domain])

 ## must come before we use the $USE_MAINTAINER_MODE variable later
AM_MAINTAINER_MODE

## don't rerun to this point if we abort
AC_CACHE_SAVE

#
# A little cut and paste to work with old versions of pkg-config
# PKG_CHECK_EXISTS is "new" in 0.19. If writing this configure.ac from
# scratch to work with those versions, I'd just write if $PKG_CONFIG --exists,
# but adding this is easier than rewriting.
#
AC_DEFUN([HIPPO_PKG_PROG_PKG_CONFIG], [AC_PATH_PROG(PKG_CONFIG, pkg-config)])

#
# This macro is:
#
# Copyright © 2004 Scott James Remnant <scott@netsplit.com>.
#
# And licensed under the GNU General Public License
#
AC_DEFUN([HIPPO_PKG_CHECK_EXISTS],
[AC_REQUIRE([HIPPO_PKG_PROG_PKG_CONFIG])dnl
if test -n "$PKG_CONFIG" && \
    AC_RUN_LOG([$PKG_CONFIG --exists --print-errors "$1"]); then
  m4_ifval([$2], [$2], [:])
m4_ifvaln([$3], [else
  $3])dnl
fi])

# libtool versioning for libddm
#
# See http://sources.redhat.com/autobook/autobook/autobook_91.html#SEC91 for details
#

## increment if the interface has additions, changes, removals.
DDM_LT_CURRENT=0

## increment any time the source changes; set to
##  0 if you increment CURRENT
DDM_LT_REVISION=0

## increment if any interfaces have been added; set to 0
## if any interfaces have been changed or removed. removal has
## precedence over adding, so set to 0 if both happened.
DDM_LT_AGE=0

AC_SUBST(DDM_LT_CURRENT)
AC_SUBST(DDM_LT_REVISION)
AC_SUBST(DDM_LT_AGE)

AC_PROG_CC
AM_PROG_CC_C_O
AM_DISABLE_STATIC
AC_PROG_CXX
AC_PROG_LIBTOOL
AC_ISC_POSIX
AC_HEADER_STDC

## don't rerun to this point if we abort
AC_CACHE_SAVE

#### gcc warning flags

changequote(,)dnl
addCommonWarnings() {
  result="$@"

  case " $result " in
  *[\ \	]-Wall[\ \	]*) ;;
  *) result="$result -Wall" ;;
  esac

  case " $result " in
  *[\ \	]-Wchar-subscripts[\ \	]*) ;;
  *) result="$result -Wchar-subscripts" ;;
  esac

  case " $result " in
  *[\ \	]-Wpointer-arith[\ \	]*) ;;
  *) result="$result -Wpointer-arith" ;;
  esac

  case " $result " in
  *[\ \	]-Wcast-align[\ \	]*) ;;
  *) result="$result -Wcast-align" ;;
  esac

  case " $result " in
  *[\ \	]-Wfloat-equal[\ \	]*) ;;
  *) result="$result -Wfloat-equal" ;;
  esac

  case " $result " in
  *[\ \	]-Wsign-compare[\ \	]*) ;;
  *) result="$result -Wsign-compare" ;;
  esac

  case " $result " in
  *[\ \	]-fno-strict-aliasing[\ \	]*) ;;
  *) result="$result -fno-strict-aliasing" ;;
  esac

  if test "x$enable_ansi" = "xyes"; then
    case " $result " in
    *[\ \	]-ansi[\ \	]*) ;;
    *) result="$result -ansi" ;;
    esac

    case " $result " in
    *[\ \	]-D_POSIX_C_SOURCE*) ;;
    *) result="$result -D_POSIX_C_SOURCE=199309L" ;;
    esac

    case " $result " in
    *[\ \	]-D_BSD_SOURCE[\ \	]*) ;;
    *) result="$result -D_BSD_SOURCE" ;;
    esac

    case " $result " in
    *[\ \	]-pedantic[\ \	]*) ;;
    *) result="$result -pedantic" ;;
    esac
  fi
  if test x$enable_gcov = xyes; then
    case " $result " in
    *[\ \	]-fprofile-arcs[\ \	]*) ;;
    *) result="$result -fprofile-arcs" ;;
    esac
    case " $result " in
    *[\ \	]-ftest-coverage[\ \	]*) ;;
    *) result="$result -ftest-coverage" ;;
    esac

    ## remove optimization
    result=`echo "$result" | sed -e 's/-O[0-9]*//g'`
  fi

  echo $result
}

addCOnlyWarnings() {
  result="$@"

  case " $result " in
  *[\ \	]-Wdeclaration-after-statement[\ \	]*) ;;
  *) result="$result -Wdeclaration-after-statement" ;;
  esac

  case " $result " in
  *[\ \	]-Wmissing-declarations[\ \	]*) ;;
  *) result="$result -Wmissing-declarations" ;;
  esac

  case " $result " in
  *[\ \	]-Wmissing-prototypes[\ \	]*) ;;
  *) result="$result -Wmissing-prototypes" ;;
  esac

  case " $result " in
  *[\ \	]-Wnested-externs[\ \	]*) ;;
  *) result="$result -Wnested-externs" ;;
  esac

  echo $result
}

addCXXOnlyWarnings() {
  result="$@"

  case " $result " in
  *[\ \	]-Wno-non-virtual-dtor[\ \	]*) ;;
  *) result="$result -Wno-non-virtual-dtor" ;;
  esac

  echo $result
}
changequote([,])dnl


if test "x$GCC" = "xyes"; then
  CFLAGS="`addCommonWarnings $CFLAGS`"
  CFLAGS="`addCOnlyWarnings $CFLAGS`"
  CXXFLAGS="`addCommonWarnings $CXXFLAGS`"
  CXXFLAGS="`addCXXOnlyWarnings $CXXFLAGS`"
else
  if test x$enable_gcov = xyes; then
    AC_MSG_ERROR([--enable-gcov can only be used with gcc])
  fi
fi

AC_SUBST(CFLAGS)
AC_SUBST(CXXFLAGS)
AC_SUBST(LDFLAGS)

# res_init() can be used to work around the inability to see changes
# in /etc/resolv.conf. We need to use AC_TRY_LINK since on GNU libc
# it's a #define for for __res_init
hippo_save_LIBS="$LIBS"
LIBS="-lresolv $LIBS"

AC_MSG_CHECKING(for res_init() in -lresolv)
AC_TRY_LINK([#include <resolv.h>], [
res_init();
], have_res_init=yes, have_res_init=no)
AC_MSG_RESULT($have_res_init)

if test "$have_res_init" = "yes"; then
    RESOLV_LIBS="-lresolv"
    AC_DEFINE(HAVE_RES_INIT, 1, [Define to 1 if you have res_init()])
fi

LIBS="$hippo_save_LIBS"

AC_ARG_WITH(maemo,
	    AC_HELP_STRING([--with-maemo=[yes/no]],
		           [Whether to use Maemo version of Mugshot client]),
	    ,
	    with_maemo=no)

GLIB2_REQUIRED=2.6.0
GTK2_REQUIRED=2.6.0
LOUDMOUTH_REQUIRED=1.2.2
# unfortunately this breaks us on FC4
DBUS_REQUIRED=0.60
DBUS_GLIB_REQUIRED=0.60
# earlier might work, but this is what's been tested
PCRE_REQUIRED=6.3
# earlier might work, but this is what's been tested
SQLITE_REQUIRED=3.3
# 2.10 for gnome_desktop_item_set_launch_time()
GNOME_DESKTOP_REQUIRED=2.10.0

XSCREENSAVER_PACKAGES=""
XSCREENSAVER_LIBS=""
XSCREENSAVER_CFLAGS=""

# First check to see if we have a .pc file for the xscreensaver extensio
HIPPO_PKG_CHECK_EXISTS(xscrnsaver, have_xscreensaver=true, have_xscreensaver=false)
if $have_xscreensaver ; then
    XSCREENSAVER_PACKAGES="xscrnsaver"
else
    # No, check the old way
    AC_PATH_XTRA
    if test "x$no_x" = xyes ; then
        AC_MSG_ERROR([Can't find path to the X development files])
    fi

    hippo_save_CPPFLAGS="$CPPFLAGS"
    CPPFLAGS="$CPPFLAGS $X_CFLAGS"

    hippo_save_LIBS="$LIBS"
    LIBS="$X_LIBS $LIBS"

    have_xscreensaver=true
    AC_CHECK_HEADERS([X11/extensions/scrnsaver.h], :, [have_xscreensaver=false])
    AC_CHECK_LIB(Xss, XScreenSaverQueryExtension, :, [have_xscreensaver=false], -lXext -lX11 $X_EXTRA_LIBS)

    CFLAGS="$hippo_save_CFLAGS"
    LIBS="$hippo_save_LIBS"

    if test "x$with_maemo" != xyes ; then        
        if ! $have_xscreensaver ; then
            AC_MSG_ERROR([XScreenSaver extension is required - X11/extensions/scnsaver.h, libXss.so])
        fi
        XSCREENSAVER_LIBS="-lXss -lXext -lX11 $X_EXTRA_LIBS"
        XSCREENSAVER_CFLAGS="$X_CFLAGS"
    else
        XSCREENSAVER_LIBS=""
        XSCREENSAVER_CFLAGS=""
    fi
fi

# PCRE checks
AC_MSG_CHECKING([for PCRE])
HIPPO_PKG_CHECK_EXISTS(libpcre >= $PCRE_REQUIRED, found_pcre=yes, found_pcre=no)
AC_MSG_RESULT([$found_pcre])
if test $found_pcre = yes ; then
   PCRE_MODULES=libpcre
   AC_DEFINE(HAVE_PCRE, 1, [Define if you have the PCRE regular expression library])
else 
   # We currently only use autoconf on platforms where we need PCRE (PCRE is used
   # in the application-tracking support to convert titles to application IDs). If
   # that changed, then this error could be removed.
   AC_MSG_ERROR([PCRE is required])
fi

# SQLite checks

AC_ARG_WITH(sqlite,
	    AC_HELP_STRING([--without-sqlite],
		           [Disable use of SQLite for caching and Firefox integration]),
	    ,
	    with_sqlite=yes)
	    
AC_MSG_CHECKING([for sqlite3])
if test x"$with_sqlite" = xno ; then
   found_sqlite=no
   AC_MSG_RESULT(disabled)
else
   HIPPO_PKG_CHECK_EXISTS(sqlite3 >= $SQLITE_REQUIRED, found_sqlite=yes, found_sqlite=no)
   AC_MSG_RESULT([$found_sqlite])
   
   if test $found_sqlite = yes ; then
      SQLITE_MODULES=sqlite3
      AC_DEFINE(HAVE_SQLITE, 1, [Define if you have SQLite database library])
   else
      if test x"$with_sqlite" = xyes ; then
         AC_MSG_ERROR([SQLite >= $SQLITE_REQUIRED not found])
      fi
   fi
fi

AM_CONDITIONAL(HAVE_SQLITE, test "x$found_sqlite" = xyes)

# If we're building for Maemo, we don't need gnome-desktop
if test "x$with_maemo" != xno ; then
   :
else
   extra_engine_packages="gnome-desktop-2.0 >= $GNOME_DESKTOP_REQUIRED"
fi

# These are fixed values for Linux; we are still using 1.0 for our Windows builds
AC_DEFINE(HAVE_LOUDMOUTH_12, 1, [Define if you have Loudmouth 1.2 or newer])
if false ; then
   AC_DEFINE(HIPPO_LOUDMOUTH_IS_10, 0, [Define if Loudmouth is version 1.0 or below])
fi

# Check for libempathy
AC_ARG_WITH(empathy,
	    AC_HELP_STRING([--without-empathy],
		           [Disable Empathy support]),
	    ,
	    with_empathy=yes)

AC_MSG_CHECKING([for empathy])
if test x"$with_empathy" = xno ; then
   found_empathy=no
   AC_MSG_RESULT(disabled)
else
   HIPPO_PKG_CHECK_EXISTS(libempathy, found_empathy=yes, found_empathy=no)
   AC_MSG_RESULT([$found_empathy])
   extra_engine_packages="$extra_engine_packages libempathy"

   if test $found_empathy = yes ; then
      EMPATHY_MODULES=empathy
      AC_DEFINE(HAVE_EMPATHY, 1, [Define if you have the empathy library])
   else
      if test x"$with_empathy" = xyes ; then
         AC_MSG_ERROR([Empathy not found])
      fi
   fi
fi

PKG_CHECK_MODULES(LIBDDM, gobject-2.0 >= $GLIB2_REQUIRED dbus-glib-1 >= $DBUS_REQUIRED)
PKG_CHECK_MODULES(LIBHIPPO, gobject-2.0 >= $GLIB2_REQUIRED gthread-2.0)
PKG_CHECK_MODULES(LIBENGINE, gobject-2.0 >= $GLIB2_REQUIRED gthread-2.0 loudmouth-1.0 >= $LOUDMOUTH_REQUIRED $PCRE_MODULES $SQLITE_MODULES)
PKG_CHECK_MODULES(DESKTOP_DATA_ENGINE, gtk+-2.0 >= $GTK2_REQUIRED gthread-2.0 loudmouth-1.0 >= $LOUDMOUTH_REQUIRED dbus-1 >= $DBUS_REQUIRED dbus-glib-1 >= $DBUS_GLIB_REQUIRED gnome-vfs-2.0 $XSCREENSAVER_PACKAGES $extra_engine_packages)

DESKTOP_DATA_ENGINE_LIBS="$DESKTOP_DATA_ENGINE_LIBS $XSCREENSAVER_LIBS $RESOLV_LIBS"
DESKTOP_DATA_ENGINE_CFLAGS="$DESKTOP_DATA_ENGINE_CFLAGS $XSCREENSAVER_CFLAGS"
AC_SUBST(DESKTOP_DATA_ENGINE_LIBS)
AC_SUBST(DESKTOP_DATA_ENGINE_CFLAGS)

if test "x$with_maemo" != xno ; then
    AC_DEFINE(WITH_MAEMO, , [whether Maemo is active])
fi
AM_CONDITIONAL(WITH_MAEMO, test "x$with_maemo" != xno)

GLIB_GENMARSHAL=`$PKG_CONFIG --variable=glib_genmarshal glib-2.0`
AC_SUBST(GLIB_GENMARSHAL)

GLIB_MKENUMS=`$PKG_CONFIG --variable=glib_mkenums glib-2.0`
AC_SUBST(GLIB_MKENUMS)

HIPPO_PKG_CHECK_EXISTS(dbus-1 < 1.0, have_dbus10=false, have_dbus10=true)
if $have_dbus10 ; then
    AC_DEFINE(HAVE_DBUS_1_0, , [whether we have dbus 1.0 or greater])
fi

## used for .desktop file in theory, disabled until we have actual translations
## IT_PROG_INTLTOOL([0.34.90])

## we just need these checks to get the gconf schemas stuff
AC_PATH_PROG(GCONFTOOL, gconftool-2, no)
if test x"$GCONFTOOL" = xno; then
    AC_MSG_ERROR([gconftool-2 executable not found in your path - should be installed with GConf])
fi

AM_GCONF_SOURCE_2

#### define absolute path to srcdir for debugging-only code
ABSOLUTE_TOP_SRCDIR=`cd ${srcdir} && pwd`
AC_DEFINE_UNQUOTED(ABSOLUTE_TOP_SRCDIR, "$ABSOLUTE_TOP_SRCDIR", [full path to srcdir])

AC_OUTPUT([
Makefile
version
ddm-1.pc
])

dnl ==========================================================================
echo "

        desktop-data-model $VERSION
	===========================

        prefix:                   ${prefix}
        compiler:                 ${CC}

        LIBHIPPO_CFLAGS:             ${LIBHIPPO_CFLAGS}
        LIBDDM_CFLAGS:		     ${LIBDDM_CFLAGS}
        LIBENGINE_CFLAGS:	     ${LIBENGINE_CFLAGS}
        DESKTOP_DATA_ENGINE_CFLAGS:  ${DESKTOP_DATA_ENGINE_CFLAGS}

        LIBHIPPO_LIBS:               ${LIBHIPPO_LIBS}
        LIBDDM_LIBS:                 ${LIBDDM_LIBS}
        LIBENGINE_LIBS:	             ${LIBENGINE_LIBS}
        DESKTOP_DATA_ENGINE_LIBS:    ${DESKTOP_DATA_ENGINE_LIBS}

        Now type 'make' to build $PACKAGE
"
